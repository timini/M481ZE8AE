
#include "mid_systimer.h"
#include "app_main.h"
#include "app_sysboot.h"

// --------------------------------------------------------------------------------
// A: Put your module_interface here

const module_interface_t * const g_module[] = {
	&g_module_sysboot, // EModule_sysboot must be the first one
};

const uint32_t g_module_count = sizeof(g_module) / sizeof(module_interface_t *);



uint32_t main_is_all_module_sleep(void)
{
	uint32_t is_sleep = 1;
	
	for(uint32_t i = 0; i < g_module_count; i++)
	{
		if (g_module[i]->module_is_sleep)
		{
			if (g_module[i]->module_is_sleep() == 0) // one module is not sleep
			{
				is_sleep = 0;
				break;
			}
		}
	}
	
	//is_sleep = 0; // void sleep. for debug
	return is_sleep;
}



void main_1ms_timer(void)
{
	if (mid_systimer_1ms_timeout() == 0)
		return;

	for(uint32_t i = 0; i < g_module_count; i++)
	{
		if (g_module[i]->module_1ms_timer)
		{
			g_module[i]->module_1ms_timer();
		}
	}
}


void main_evt_broadcast(EModule_t module_src, void * evt)
{
	for(uint32_t i = 0; i < g_module_count; i++)
	{
		if (g_module[i]->module_evt_handler)
		{
			g_module[i]->module_evt_handler(module_src, evt);
		}
	}
}


void *main_msg_send(EModule_t module_src, EModule_t module_dts, void * msg)
{
	void * ret = 0;
	
	for(uint32_t i = 0; i < g_module_count; i++)
	{
		if (g_module[i]->module_type == module_dts)
		{
			if (g_module[i]->module_msg_handler)
			{
				ret = g_module[i]->module_msg_handler(module_src, module_dts, msg);
				break;
			}
		}
	}

	return ret;
}


int main(void)
{
	SysEvent_t evt;

	// 事件广播: Reset
	evt.type = SysEvent_Reset;
	main_evt_broadcast(EModule_sysboot, &evt);

	// 事件广播: Pow On
	evt.type = SysEvent_PowOn;
	main_evt_broadcast(EModule_sysboot, &evt);
	
	while(1)
	{
		main_1ms_timer();
		
		//-------------------------------------
		for(uint32_t i = 0; i < g_module_count; i++)
		{
			if (g_module[i]->module_service != 0)
			{
				g_module[i]->module_service();
			}
		}
	}
}

