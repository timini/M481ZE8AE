#ifndef __app_sysboot_internal_h__
#define __app_sysboot_internal_h__

#include "app_sysboot.h"

/***********************************************************************************/

#pragma anon_unions

// 系统工作模式 定义
typedef enum{
	SysMode_Working,
	SysMode_Sleep,

	SysMode_Max
}SysMode_t;


typedef struct{
	uint32_t flag_wakeup_src; // 记录本次唤醒中断源
	uint32_t flag_reset_src;	// 记录本次复位源
	
	sysboot_powstate_t pow_cur;
	sysboot_powstate_t pow_pre;
	uint8_t pow_volt;	//当前供电电压值)，单位 0.1v

	// 规则: 以下为sysboot私有数据成员，
	SysMode_t state;
	SysMode_t state_prev; // 上一个状态
	uint16_t time_state;
}Sysboot_service_define;

uint32_t sysboot_is_sleep(void);

#endif
