#include "M480.h"

#include "mid_clock.h"
#include "mid_systimer.h"
#include "mid_debug.h"
#include "mid_pwm0.h"
#include "mid_spi0.h"

#include "app_sysboot.h"
#include "app_sysboot_internal.h"
#include "types.h"
#include "mid_terminal.h"

Sysboot_service_define g_sysboot_service;

// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// Sysboot调试信息输出    Log开关

#define LogMask_Sysboot  ((ELOG_Error | ELOG_Warning | ELOG_Trace | ELOG_Info) & LogMask_Globle)

#define sysboot_log(type,tag, formate, ...) if ((type) & LogMask_Sysboot) Mid_Debug_Sprintf(type, tag, formate, ##__VA_ARGS__)


void sysboot_reset_info(void)
{
	sysboot_log(ELOG_Trace,"Reset","\r\n\r\n======== System Reset !!! ========\r\n");
	
    uint32_t reset_flags = SYS->RSTSTS;
//    if (reset_flags & RCM_SRS_LVD_MASK)    sysboot_log(ELOG_Trace,"Reset","Reset caused by LVD trip, HVD trip or POR\r\n");
//    if (reset_flags & RCM_SRS_LOC_MASK)    sysboot_log(ELOG_Trace,"Reset","Reset caused by a loss of external clock.\r\n");
//    if (reset_flags & RCM_SRS_LOL_MASK)    sysboot_log(ELOG_Trace,"Reset","Reset caused by a loss of lock in the PLL/FLL\r\n");
//
//    if (reset_flags & RCM_SRS_WDOG_MASK)   sysboot_log(ELOG_Trace,"Reset","Reset caused by watchdog timeout\r\n");
//    if (reset_flags & RCM_SRS_PIN_MASK)    sysboot_log(ELOG_Trace,"Reset","Reset caused by external reset pin\r\n");
//    if (reset_flags & RCM_SRS_POR_MASK)    sysboot_log(ELOG_Trace,"Reset","Reset caused by POR\r\n");
//    if (reset_flags & RCM_SRS_JTAG_MASK)   sysboot_log(ELOG_Trace,"Reset","Reset caused by JTAG\r\n");
//    if (reset_flags & RCM_SRS_LOCKUP_MASK) sysboot_log(ELOG_Trace,"Reset","Reset caused by core LOCKUP event\r\n");
//    if (reset_flags & RCM_SRS_SW_MASK)     sysboot_log(ELOG_Trace,"Reset","Reset caused by software setting of SYSRESETREQ bit\r\n");
//    if (reset_flags & RCM_SRS_MDM_AP_MASK) sysboot_log(ELOG_Trace,"Reset","Reset caused by host debugger system setting of the System Reset Request bit\r\n");
//    if (reset_flags & RCM_SRS_SACKERR_MASK)sysboot_log(ELOG_Trace,"Reset","Reset caused by peripheral failure to acknowledge attempt to enter stop mode\r\n");
//
//	sysboot_log(ELOG_Trace,"Reset","\r\n\r\n");
	
	g_sysboot_service.flag_reset_src = reset_flags; // 记录本次复位源信息
}


void sysboot_wakeup_info(void)
{
//	extern Sysboot_service_define g_sysboot_service;
//	uint32_t int_flags = g_sysboot_service.flag_wakeup_int;
//	
//	sysboot_log(ELOG_Trace,"Wakeup","\r\n\r\n======== System Wakeup !!! ========\r\n");

//	if (int_flags & (1UL << 0))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved16_IRQn.\r\n");
//	if (int_flags & (1UL << 1))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved17_IRQn.\r\n");
//	if (int_flags & (1UL << 2))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved18_IRQn.\r\n");
//	if (int_flags & (1UL << 3))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved19_IRQn.\r\n");
//	if (int_flags & (1UL << 4))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved20_IRQn.\r\n");
//	if (int_flags & (1UL << 5))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By FTMRE_IRQn.\r\n");
//	if (int_flags & (1UL << 6))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By PMC_IRQn.\r\n");
//	if (int_flags & (1UL << 7))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By IRQ_IRQn.\r\n");
//	if (int_flags & (1UL << 8))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By I2C0_IRQn.\r\n");
//	if (int_flags & (1UL << 9))		sysboot_log(ELOG_Trace,"Wakeup","Wakeup By I2C1_IRQn.\r\n");

//	if (int_flags & (1UL << 10))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By SPI0_IRQn.\r\n");
//	if (int_flags & (1UL << 11))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By SPI1_IRQn.\r\n");
//	if (int_flags & (1UL << 12))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By UART0_IRQn.\r\n");
//	if (int_flags & (1UL << 13))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By UART1_IRQn.\r\n");
//	if (int_flags & (1UL << 14))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By UART2_IRQn.\r\n");
//	if (int_flags & (1UL << 15))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By ADC_IRQn.\r\n");
//	if (int_flags & (1UL << 16))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By ACMP0_IRQn.\r\n");
//	if (int_flags & (1UL << 17))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By FTM0_IRQn.\r\n");
//	if (int_flags & (1UL << 18))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By FTM1_IRQn.\r\n");
//	if (int_flags & (1UL << 19))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By FTM2_IRQn.\r\n");

//	if (int_flags & (1UL << 20))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By RTC_IRQn.\r\n");
//	if (int_flags & (1UL << 21))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By ACMP1_IRQn.\r\n");
//	if (int_flags & (1UL << 22))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By PIT_CH0_IRQn.\r\n");
//	if (int_flags & (1UL << 23))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By PIT_CH1_IRQn.\r\n");
//	if (int_flags & (1UL << 24))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By KBI0_IRQn.\r\n");
//	if (int_flags & (1UL << 25))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By KBI1_IRQn.\r\n");
//	if (int_flags & (1UL << 26))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By Reserved42_IRQn.\r\n");
//	if (int_flags & (1UL << 27))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By ICS_IRQn.\r\n");
//	if (int_flags & (1UL << 28))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By WDOG_IRQn.\r\n");
//	if (int_flags & (1UL << 29))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By PWT_IRQn.\r\n");

//	if (int_flags & (1UL << 30))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By MSCAN_RX_IRQn.\r\n");
//	if (int_flags & (1UL << 31))	sysboot_log(ELOG_Trace,"Wakeup","Wakeup By MSCAN_TX_IRQn.\r\n");

//    sysboot_log(ELOG_Trace,"Reset","\r\n\r\n");
}


void sysboot_state_info(void)
{
	extern Sysboot_service_define g_sysboot_service;
	extern const module_interface_t * g_module[];
	extern const uint32_t g_module_count;

	// 输出系统当前状态
   
	//输出adc1各通道采样值
	//mid_adc1_dump();

	// 输出第一个阻止系统休眠的模块
	for(uint32_t i = 0; i < g_module_count; i++)
	{
		if (g_module[i]->module_is_sleep == 0)
			continue;

		if (g_module[i]->module_is_sleep() == 0)
		{
			sysboot_log(ELOG_Trace,"Sysboot",(uint8_t *)g_module[i]->module_name);
		}
	}
}


// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// sysboot模块


void sysboot_sleep(void)
{
//	mid_clock_powerdown();
}


void sysboot_pin_init(void)
{
	//hal_gpio_reset();
}


void sysboot_var_init(void)
{	g_sysboot_service.pow_cur = EPowState_null;
	g_sysboot_service.pow_pre = EPowState_null;
	g_sysboot_service.pow_volt = 0;
	
}


uint32_t sysboot_init(void)
{
	sysboot_var_init();
	
	mid_clock_init();
	mid_systimer_init();
	PWM0_Init(4000000,50);
	SPI0_Init();
	
	mid_debug_init();
	mid_terminal_init(0);
	//sysboot_wakeup_info();	// 输出唤醒信息
	return 0;
}


void sysboot_deinit(void)
{
    sysboot_log(ELOG_Trace,"PowOff","\r\n\r\n======== System PowOff !!! ========\r\n");
    
    PWM0_DeInit();
	SPI0_DeInit();
	
	mid_debug_deinit();
	mid_terminal_deinit();
	mid_systimer_deinit();
	mid_clock_deinit(); //时钟要在最后关	
}


void sysboot_sysevent_handler(EModule_t module_src, void *p_event)
{
	if (module_src == EModule_sysboot)
	{
		SysEvent_t * evt = (SysEvent_t*)p_event;
		switch(evt->type)
		{
			case SysEvent_Reset:
				g_sysboot_service.state = SysMode_Working;
				sysboot_reset_info();
				sysboot_pin_init();
			break;
			
			case SysEvent_PowOn:
				sysboot_init();
			break;

			case SysEvent_PowDown:
			break;

			default:
			break;
		}
	}
}

void* sysboot_message_handler(EModule_t module_src, EModule_t module_dts, void *p_message)
{
	return (void *)E_NOSPT;
}


void sysboot_10msTimer(void)
{
//    mid_adc1_10mstimer();
}

void sysboot_1msTimer(void)
{
	static uint8_t tick_10ms_timer;
	
	if (g_sysboot_service.time_state < 0xFFFF)
		g_sysboot_service.time_state++;

	tick_10ms_timer ++;
	if (tick_10ms_timer >= 10)
	{
		tick_10ms_timer = 0;
		sysboot_10msTimer();
	}
}

uint32_t sysboot_is_sleep(void)
{
	return 0;
}


void sysboot_service(void)
{
	SysMode_t state_backup;
	SysEvent_t evt;
	uint32_t enter_sleep;
//	uint8_t sleep_status = 0; 

	state_backup = g_sysboot_service.state;
	switch (g_sysboot_service.state)
	{
		case SysMode_Working:
			// 1. 进行各种 失效检测
			mid_terminal_service();
			
			// 2. 每1s询问一次是否可以休眠
			if (g_sysboot_service.time_state >= 1000)
			{
				SPI0_Test();
				g_sysboot_service.time_state = 0;
				enter_sleep = main_is_all_module_sleep();
				if (enter_sleep)
				{
					//if (mid_eeprom_is_sleep())
					{
						g_sysboot_service.state = SysMode_Sleep;
					}
				}
				sysboot_state_info();
			}
		break;
		
		case SysMode_Sleep:
			if (g_sysboot_service.time_state < 1000)
				break;
			
			// [事件: 下电事件]
			evt.type = SysEvent_PowDown;
			main_evt_broadcast(EModule_sysboot, (void*)&evt);

			sysboot_deinit();
			//nvic_clear_interrupt_allflags();
			//nvic_clear_interrupt_allcounts();
			
			// 初始化rtc, 系统休眠 ...
			//while(1)
			{
				sysboot_sleep();
			}

			// [事件: 上电事件]
			evt.type = SysEvent_PowOn;
			main_evt_broadcast(EModule_sysboot, (void*)&evt);
			
//			g_sysboot_service.flag_wakeup_int = nvic_get_interrupt_flags(); // 记录本次唤醒源信息
			g_sysboot_service.state = SysMode_Working;
		break;

		default:
			g_sysboot_service.state = SysMode_Working;
		break;
	}

	if (state_backup != g_sysboot_service.state) // 状态发生了迁移
	{
		g_sysboot_service.state_prev = state_backup;
		g_sysboot_service.time_state = 0;
	}
}

//--------------------------------------------------------------------------------------
// define module interface

const module_interface_t g_module_sysboot = {
	.module_type = EModule_sysboot,
	.module_name = "EModule_sysboot",
	
	.module_1ms_timer = sysboot_1msTimer,
	.module_evt_handler = sysboot_sysevent_handler,
	.module_msg_handler = sysboot_message_handler,
	
	.module_is_sleep = sysboot_is_sleep,
	.module_service = sysboot_service,
	.module_back_service = 0,
};

