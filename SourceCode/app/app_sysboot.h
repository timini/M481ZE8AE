/**********************************************************************************
 *@file    : app_sysboot.h 
 *@brief   : this file is a interface for other module(mid or app),
 *           please contain necessary detials and keep it simple. 
 *@author  : liucheng
 *@version : Ver 1.1
 *@data    : 2018.01.03
 *@note    : Copyright(c) 2014 PROTRULY. All rights reserved.
 ***********************************************************************************/

#ifndef __app_sysboot_h__
#define __app_sysboot_h__

#include <stdint.h>
#include "app_main.h"

#pragma anon_unions

/***********************************************************************************/


// 电源电压状态 定义
typedef enum{
	EPowState_null	= 0,
	
	                     //         电压上升阶段              电压下降阶段
	EPowState_uv2,       // UV2    0.0v <= VIGN <  6.5v     0.0v <= VIGN  <  6.0v
	EPowState_uv1,       // UV1	   6.5v <= VIGN <  9.0v     6.0v <= VIGN  <  8.5v
	EPowState_v,         // V      9.0v <= VIGN < 16.5v     8.5v <= VIGN  < 16.0v
	EPowState_ov1,       // OV1	  16.5v <= VIGN < 18.5v	   16.0v <= VIGN  ≤18.0v
	EPowState_ov2,       // OV2   18.5V <= VIGN            18.0v <= VIGN

	
	EPowState_max,
}sysboot_powstate_t;


typedef enum{
	EVideoSrc_null	= 0,
	
	EVideoSrc_nv,
	EVideoSrc_dvr,

	EVideoSrc_max
}sysboot_videosource_t;


/***********************************************************************************/
// sysboot message定义

typedef enum{
	ESysMsg_can_acc,


	ESysMsg_unknown
}sysboot_msg_type_t;


typedef union{
	// type == xxx 时的参数 
	// ...
	uint8_t data;
}sysboot_msg_param_t;


typedef struct{
	sysboot_msg_type_t type;
	sysboot_msg_param_t param;
}sysboot_msg_t;


/***********************************************************************************/
// sysboot 事件结构体定义

typedef enum{
	SysEvent_Null = 0,

	// sysboot 事件类型
	SysEvent_Reset,			// 系统复位后触发,用于各模块初始化与之相关的全局变量
	SysEvent_PowOn,			// CPU唤醒后触发, 用于各模块初始化与之相关的硬件单元
	SysEvent_PowDown,		// CPU休眠前触发, 用于各模块关闭与之相关的硬件单元，以降低功耗
	
	SysEvent_ChangePowState,	// powstate状态变化时 触发

	SysEvent_Max
}sysboot_evt_type_t;


typedef union{
	struct{
		sysboot_powstate_t pow_prev;	// 从哪个 电压状态 迁出的
		sysboot_powstate_t pow_cur;	// 当前的 电压状态
		uint8_t pow_volt;	// 当前电压值, 精度0.1v
	};

	uint8_t * pdata;
}sysboot_evt_param_t;

typedef struct
{
	sysboot_evt_type_t type;
	sysboot_evt_param_t param;
}SysEvent_t;


/***********************************************************************************/
// sysboot 对外接口

extern const module_interface_t g_module_sysboot;

#endif

