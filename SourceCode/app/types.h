#ifndef __types_h__
#define __types_h__

#include <stdbool.h>
#include "stdint.h"
#include "string.h"
#include "assert.h"

#define MCU_VERSION  "Ver0.806"  //

#define DisableInterrupts	__asm("cpsid i");
#define EnableInterrupts	__asm("cpsie i");

// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// log相关定义

#define LogMask_Globle		(ELOG_Error | ELOG_Warning | ELOG_Trace | ELOG_Info | ELOG_Debug | ELOG_Test)

typedef enum {
	ELOG_Assert     = 0x01,
	ELOG_Error      = 0x02,
	ELOG_Warning    = 0x04,
	ELOG_Trace      = 0x08,
	ELOG_Info       = 0x10,
	ELOG_Debug      = 0x20,
	ELOG_Test		= 0x40,

	ELOG_Max
}ELogType_t;

#define Str_ELOG_Assert     "assert"
#define Str_ELOG_Error      "ERROR"
#define Str_ELOG_Warning    "WARNING"
#define str_ELOG_Trace      "TRACE"
#define Str_ELOG_Info       "INFO"
#define Str_ELOG_Debug      "Debug"
#define Str_ELOG_Test       "Test"

#define Str_ELOG_Unknown    "Unknown"


#define NLOG

// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// 基本数据类型定义

typedef enum{
	E_OK			=(0),		/* No Error						*/

	E_NOT_OK		=(2),		/* data error		 				*/
	E_DATAERR		=(3),		/* data error		 				*/
	E_WAITTIMEOUT	=(4),		/* wait time out	 				*/
	E_SYS			=(5),		/* System error: Unknown cause		*/
	E_POINT			=(6),		/* param point is null				*/
	E_MSGFULL		=(7),		/* msg of buffer is full			*/
	E_NODATA		=(8),		/* no data						*/
	E_NOMSG			=(9),		/* no message						*/
	E_NOMEM			=(10),		/* Insufficient Memory 			*/
	E_BUFOVERFLOW	=(11),		/* buffer data overflow 			*/
	E_NOCMDACK		=(12),		/* no cmd ack	 data 				*/
	E_NOACKERROR	=(13),		/* ack data error 					*/
	E_ACKTIMEOUT	=(14),		/* ack time out	 				*/
	E_WAIT			=(15),		/* wait status		 			*/
	E_CONTINUE		=(16),		/* keep wait 		 				*/

	E_NOSPT			=(17),		/* Unsupported Feature				*/
	E_INOSPT		=(18),		/* os dont's supported Feature		*/
	E_RSFN			=(20),		/* Reserved Function Code Number	*/
	E_RSATR			=(24),		/* Reserved Attribute				*/
	E_PAR			=(33),		/* Parameter error					*/
	E_ID			=(35),		/* Invalid ID number				*/
	E_NOEXS			=(52),		/* Object does not exist			*/
	E_OBJ			=(63),		/* Invalid object state			*/
	E_MACV			=(65),		/* Memory access disable/violation	*/
	E_OACV			=(66),		/* Object access violation			*/
	E_CTX			=(69),		/* Context error					*/
	E_QOVR			=(73),		/* Queuing or nesting overflow		*/
	E_DLT			=(81),		/* Waiting object deleted			*/
	E_TMOUT			=(85),		/* Polling failure or timeout		*/
	E_RLWAI			=(86),		/* wait state forcibly released		*/

	EN_NOND			=(113),		/* Target node does not exist		*/
	EN_OBJNO		=(114),		/* Object could not be accessed		*/
	EN_PROTO		=(115),		/* No target support of Protocol	*/
	EN_RSFN			=(116),		/* No target support of Systemcall	*/
	EN_COMM			=(117),		/* No response from target			*/
	EN_RLWAI		=(118),		/* Connection WAIT forced release	*/
	EN_PAR			=(119),		/* Out of range paramter			*/
	EN_RPAR			=(120),		/* Out of range return parameter	*/
	EN_CTXID		=(121),		/* SEE SPECIFICATION				*/
	EN_EXEC			=(122),		/* Insuf. resources for Systemcall 	*/
	EN_NOSPT		=(123),		/* Connection fn not supported		*/

	EV_FULL			=(225)		/* NOT IN SPECIFICATION			*/
	
}Enum_Result_t;

#define  BOOL unsigned char
//#define FALSE 0
//#define TRUE 1
#define ON		1
#define OFF     0
//
//#define ENABLE     1
//#define DISABLE    0
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// 位域

#define setBit(x,y) 			(x) = (x)|(1<<(y))
#define clrBit(x,y) 			(x) = (x)&(~(1<<(y)))

//////////////////////////Basical Handle//////////////////////////////
#define 		ABS(x)										(((x) >= 0) ? (x) : -(x))
#define 		DIM(x) 										(sizeof(x) / sizeof(x[0]))
#define 		MAKEWORD(a, b)      						((uint16_t)(((uint8_t)(a)) | ((uint16_t)((uint8_t)(b))) << 8))
#define 		MAKELONG(a, b)      						((uint32_t)(((uint16_t)(a)) | ((uint32_t)((uint16_t)(b))) << 16))
#define 		LOWORD(L)           						((uint16_t)(L))
#define 		HIWORD(L)           						((uint16_t)(((uint32_t)(L) >> 16) & 0xFFFF))
#define 		LOBYTE(w)           						((0xff)&(w))
#define 		HIBYTE(w)           						(((uint16_t)(w) >> 8) & 0xFF)

#define 		U32_BTYE0(L)           						((uint8_t)(L) & 0xff)		//bit0 - bit7
#define 		U32_BTYE1(L)           						((uint8_t)(L >> 8) & 0xff)	//bit8 - bit15
#define 		U32_BTYE2(L)           						((uint8_t)(L >> 16) & 0xff)	//bit16 - bit23
#define 		U32_BTYE3(L)           						((uint8_t)(L >> 24) & 0xff)	//bit24 - bit31

#define 		SWAP(a, b)           						{((a)^=(b)); ((b)^=(a)); ((a)^=(b));}

#define			DIV_FILL_ALIGN(length, size)				(((length) + (size) - 1)/(size))
#define			DIV_CUT_ALIGN(length, size)					((length)/(size))
//Avoid compiler warnings about unreferenced parameters
#define 		UNUSED_PARAM(P)									(P = P)

#define BUILD_UINT16(Byte1, Byte0) \
          ((uint16_t)((uint32_t)((Byte0) & 0x00FF) + ((uint32_t)((Byte1) & 0x00FF) << 8)))



#define BUILD_UINT32(Byte3,Byte2,Byte1,Byte0) \
          ((uint32_t)((uint32_t)((Byte0) & 0x00FF) \
          + ((uint32_t)((Byte1) & 0x00FF) << 8) \
          + ((uint32_t)((Byte2) & 0x00FF) << 16) \
          + ((uint32_t)((Byte3) & 0x00FF) << 24)))
#ifndef MIN
#define MIN(a, b)	(((a) > (b))? (b): (a))
#endif
#define LO_UINT8(a) ((a) & 0x0F)

#define DEV_ASSERT(x) ((void)0)

// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// 日期时间

typedef struct {
	uint16_t hour;
	uint8_t min;
	uint8_t sec;
	uint16_t msec;
}DateTime_t;

extern volatile DateTime_t g_systime;
extern volatile uint32_t g_systick;


// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// 环形缓冲区

#define LoopBuff_Item_CanRead(buff)		(buff.mptr |= 0xFFFF, (buff.mptr & (buff.wptr - buff.rptr)))
#define LoopBuff_Item_CanWrite(buff)	(buff.mptr |= 0xFFFF, (buff.mptr & (buff.rptr - buff.wptr - 1)))
#define LoopBuff_Is_Full(buff)			(LoopBuff_Item_CanWrite(buff) == 0)	
#define LoopBuff_Is_Empty(buff)			(LoopBuff_Item_CanRead(buff) == 0)

#define LoopBuff_Read_Item(buff)		(buff.data[buff.rptr++])
#define LoopBuff_Write_Item(buff,item)	{buff.data[buff.wptr++] = item;}
#define LoopBuff_Clear_All(buff)		{buff.rptr = buff.wptr = 0;}

typedef struct{
	uint8_t mptr :4;
	uint8_t rptr :4;
	uint8_t wptr :4;

	uint8_t data[16];
}LoopBuf_u8_16_Define;

typedef struct{
	uint8_t mptr	 :5;
	uint8_t rptr :5;
	uint8_t wptr :5;

	uint8_t data[32];
}LoopBuf_u8_32_Define;

typedef struct{
	uint8_t mptr	 :6;
	uint8_t rptr :6;
	uint8_t wptr :6;

	uint8_t data[64];
}LoopBuf_u8_64_t;

typedef struct{
	uint8_t mptr	 :6;
	uint8_t rptr :6;
	uint8_t wptr :6;

	uint16_t data[64];
}LoopBuf_u16_64_Define;

typedef struct{
	uint8_t mptr	 :7;
	uint8_t rptr :7;
	uint8_t wptr :7;

	uint8_t data[128];
}LoopBuf_u8_128_t;

typedef struct{
	uint8_t mptr	 :7;
	uint8_t rptr :7;
	uint8_t wptr :7;

	uint8_t data[128];
}LoopBuf_u8_128_Define;

typedef struct{
	uint8_t mptr;
	uint8_t rptr;
	uint8_t wptr;

	uint8_t data[256];
}LoopBuf_u8_256_Define;

typedef struct{
	uint16_t mptr :9;
	uint16_t rptr :9;
	uint16_t wptr :9;

	uint8_t data[512];
}LoopBuf_u8_512_Define;

typedef struct{
	uint16_t mptr :10;
	uint16_t rptr :10;
	uint16_t wptr :10;

	uint8_t data[1024];
}LoopBuf_u8_1024_Define;

typedef struct{
	uint16_t mptr :10;
	uint16_t rptr :10;
	uint16_t wptr :10;

	uint16_t data[1024];
}LoopBuf_u16_1024_Define;


typedef struct{
	uint16_t mptr :11;
	uint16_t rptr :11;
	uint16_t wptr :11;

	uint8_t data[2048];
}LoopBuf_u8_2048_Define;

typedef struct{
	uint16_t mptr :12;
	uint16_t rptr :12;
	uint16_t wptr :12;

	uint8_t data[4096];
}LoopBuf_u8_4096_Define;


/***********************************************************************************/
// 模块枚举 & 模块名称定义

//typedef enum {
//	EModule_sysboot,
//	EModule_arm,
//	EModule_can,
//	EModule_max
//}EModule_t;

//#define Emodule_t_String\
//		"EModule_sysboot",\
//		"EModule_arm",\
//		"EModule_can"

/***********************************************************************************/
// 模块interface 结构体

//typedef void (*void_func_t)(void);	
//typedef void (*void_event_handler)(EModule_t module_src, uint8_t *p_event);
//typedef uint32_t (*void_message_handler)(EModule_t module_src, EModule_t module_desc, uint8_t *p_message);
//typedef uint32_t (*bool_func_t)(void);


//typedef struct {
//	EModule_t   module_id;
//	void_func_t module_1mstimer;
//	void_func_t module_10mstimer;

//	void_event_handler module_event_handler;
//	void_message_handler module_message_handler;
//	void_func_t module_service;

//	bool_func_t module_is_sleep;

//}Module_Interface_t;



/***********************************************************************************/

//uint32_t mid_syscount_getcount(void);
//uint32_t mid_syscount_calcdeta(uint32_t count_end, uint32_t count_start);




#endif //#ifndef __types_h__

