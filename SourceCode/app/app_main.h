/********************************************************************************/
/*@file		:	main.h															*/
/*@brief    :	main head file													*/
/*@author   :	xxxxxxx														*/
/*@version	:	Ver0.80															*/
/*@data    	:	2014-08-23														*/
/*@note		:	Copyright(c) 2014 PROTRULY. All rights reserved.				*/
/********************************************************************************/
#ifndef _MAIN_H_ 
#define _MAIN_H_

#include "stdint.h"


//---------------------------------------------------------------------------------------------------

typedef enum{
	EModule_sysboot,

	EModule_arm,
	EModule_tof,
	EModule_display,
	EModule_mscan,
	EModule_diag,
	EModule_backmain,

	EModule_max
}EModule_t;


typedef void (*func_voidfunc_t)(void);
typedef void (*func_evt_handle_t)(EModule_t module_src, void *);
typedef void *(*func_msg_handle_t)(EModule_t module_src, EModule_t module_dts, void * msg);
typedef uint32_t (*func_is_sleep_t)(void);

typedef struct{
	EModule_t        module_type;
	const char *         module_name;
	
	func_voidfunc_t      module_1ms_timer;
	func_evt_handle_t    module_evt_handler;
	func_msg_handle_t    module_msg_handler;

	
	func_is_sleep_t      module_is_sleep;
	func_voidfunc_t      module_service;
	func_voidfunc_t      module_back_service;
}module_interface_t;


//---------------------------------------------------------------------------------------------------
// main.c's interface for modules use


uint32_t main_is_all_module_sleep(void);

void main_1ms_timer(void);

void main_evt_broadcast(EModule_t module_src, void * evt);

void *main_msg_send(EModule_t module_stc, EModule_t module_tds, void * msg);



#endif

