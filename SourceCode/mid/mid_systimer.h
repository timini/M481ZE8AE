/**********************************************************************************
 *@file    : hal_systime.h 
 *@brief   : this file is a interface for other module(mid or app),
 *           please contain necessary detials and keep it simple. 
 *@author  : liucheng
 *@version : Ver 1.1
 *@data    : 2018-01-16
 *@note    : Copyright(c) 2014 PROTRULY. All rights reserved.
 ***********************************************************************************/
 
#ifndef __hal_systimer_h__
#define __hal_systimer_h__

#include "stdint.h"

/**********************************************************************************/
// 1. types define

typedef struct {
	uint16_t hour;
	uint8_t min;
	uint8_t sec;
	uint16_t msec;
}mid_systime_t;


/**********************************************************************************/
// operations define

extern volatile uint32_t g_systick;
void mid_systimer_get_systime(mid_systime_t * systime);


void mid_systimer_init(void);					// only for sysboot
void mid_systimer_deinit(void);				// only for sysboot
void mid_systimer_delay(uint16_t ms);        //only for test

uint32_t mid_systimer_1ms_timeout(void);	// only for sysboot


#endif

