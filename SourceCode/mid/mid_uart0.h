#ifndef __mid_uart0_h__
#define __mid_uart0_h__

#include "types.h"

void UART0_EnableTxInt(uint8_t enable);
void UART0_Init(void);
void UART0_DeInit(void);
uint8_t UART0_ReadByte(uint8_t * data);
uint8_t UART0_WriteByte(uint8_t data);
uint8_t UART0_Read(uint8_t data[], uint8_t data_len);
uint16_t UART0_Write(uint8_t data[], uint8_t data_len);

void UART0_Test(void);

#endif


