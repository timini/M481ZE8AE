#include "mid_systimer.h"
#include "M480.h"


volatile uint32_t g_systick;
volatile uint32_t g_apptick;	// for debug 如果主循环运行足够快， g_app_tick与g_sys_tick保持一致,否则g_app_tick滞后于g_sys_tick;

// -----------------------------------------------------------------------------------------------------
// systick 硬件

void systick_init(void)
{
	uint32_t core_freq = 0u;

	core_freq = CLK_GetCPUFreq();
	SysTick_Config(core_freq/1000);
	
	g_systick = 0;
	g_apptick = 0;
}

void systick_deinit(void)
{
	//TODO:关闭systick
	g_systick = 0;
	g_apptick = 0;
}

void SysTick_Handler(void)
{
	g_systick++;
}


// -----------------------------------------------------------------------------------------------------
// systick mid层

void mid_systimer_get_systime(mid_systime_t * systime)
{
	uint32_t time = g_systick; // time uint is changed to: ms

	systime->msec = time % 1000;

	time = time / 1000; // time uint is changed to: sec
	systime->sec = time % 60;

	time = time / 60;   // time uint is changed to: min
	systime->min = time % 60;

	time = time / 60;   // time uint is changed to: hour
	systime->hour = time;
}

uint32_t mid_systimer_1ms_timeout(void)
{
	static uint32_t last_tick = 0;

	if (last_tick == g_systick)
	{
		return 0;
	}
	
	last_tick = g_systick;
	g_apptick++;
	return 1;
}

void mid_systimer_init(void)
{
	systick_init();
}

void mid_systimer_deinit(void)
{
	systick_deinit();
}


void mid_systimer_delay(uint16_t ms)
{
	uint32_t tick_timeout = g_systick + ms + 1;
	while(g_systick != tick_timeout)
	{
	}
}



