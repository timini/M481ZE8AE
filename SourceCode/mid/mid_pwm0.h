#ifndef __mid_pwm0_h__
#define __mid_pwm0_h__

#include "stdint.h"


uint32_t PWM0_Init(uint32_t freq, uint8_t duty);
uint32_t PWM0_DeInit(void);
uint32_t PWM0_SetDuty(uint32_t duty);


#endif

