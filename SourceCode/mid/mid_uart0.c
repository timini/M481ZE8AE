#include "mid_uart0.h"
#include "mid_clock.h"


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// UART1 硬件驱动

LoopBuf_u8_4096_Define g_UART1_Tx_Buff;
LoopBuf_u8_128_Define g_UART1_Rx_Buff;
uint8_t g_UART1_Inited;

void UART1_EnableTxInt(uint8_t enable) 
{
	if(enable) 
		UART_EnableInt(UART1, UART_INTEN_THREIEN_Msk); 
	else 
		UART_DisableInt(UART1, UART_INTEN_THREIEN_Msk); 
}


void UART1_init(void)
{
    // Pin Config
    GPIO_SetMode(PB, BIT2, 6);//PB.2 UART1_RXD I MFP6 UART1 data receiver input pin.
    GPIO_SetMode(PB, BIT3, 6);//PB.3 UART1_TXD O MFP6 UART1 data transmitter output pin.
	
	/* Configure UART1 and set UART1 baud rate */
    UART_Open(UART1, 115200);
    
	NVIC_ClearPendingIRQ(UART1_IRQn);
	NVIC_EnableIRQ(UART1_IRQn);
}
/*---------------------------------------------------------------------------------------------------------*/
/* ISR to handle UART Channel 0 interrupt event                                                            */
/*---------------------------------------------------------------------------------------------------------*/
void UART1_IRQHandler(void)
{
    uint8_t u8InChar = 0xFF;
    uint32_t u32IntSts = UART1->INTSTS;

    if((u32IntSts & UART_INTSTS_RDAINT_Msk) || (u32IntSts & UART_INTSTS_RXTOINT_Msk))
    {
        /* Get all the input characters */
        while(UART_GET_RX_EMPTY(UART1) == 0)
        {
            /* Get the character from UART Buffer */
            u8InChar = UART_READ(UART1);
            
            if (LoopBuff_Is_Full(g_UART1_Rx_Buff))
	    		continue;

            LoopBuff_Write_Item(g_UART1_Rx_Buff,u8InChar);
        }
    }

    if(u32IntSts & UART_INTEN_THREIEN_Msk)
    {
    	if (LoopBuff_Is_Empty(g_UART1_Tx_Buff))
        {
        	UART1_EnableTxInt(0);
        	return;
        }

        u8InChar = LoopBuff_Read_Item(g_UART1_Tx_Buff);
        UART_WRITE(UART1, u8InChar);
    }

    if(UART1->FIFOSTS & (UART_FIFOSTS_BIF_Msk | UART_FIFOSTS_FEF_Msk | UART_FIFOSTS_PEF_Msk | UART_FIFOSTS_RXOVIF_Msk))
    {
        UART1->FIFOSTS = (UART_FIFOSTS_BIF_Msk | UART_FIFOSTS_FEF_Msk | UART_FIFOSTS_PEF_Msk | UART_FIFOSTS_RXOVIF_Msk);
    }
}

void UART1_Init(void)
{
	//LoopBuff_Clear_All(g_UART1_Tx_Buff);
	//LoopBuff_Clear_All(g_UART1_Rx_Buff);

	UART1_init();
	g_UART1_Inited = 1;
}
void UART1_DeInit(void)
{
	g_UART1_Inited = 0;
	while(1){
		uint16_t usned_size = LoopBuff_Item_CanRead(g_UART1_Tx_Buff);
		
		if (usned_size == 0)
			break;
	}
	
    //TODO:关闭IO口, 关闭串口模块
  	UART_Close(UART1);
}
uint8_t UART1_ReadByte(uint8_t * data)
{
	if (LoopBuff_Is_Empty(g_UART1_Rx_Buff))
		return 0;

	*data = LoopBuff_Read_Item(g_UART1_Rx_Buff);
	return 1;
}

uint8_t UART1_WriteByte(uint8_t data)
{
	uint16_t unusned_size = LoopBuff_Item_CanWrite(g_UART1_Tx_Buff);
	if (unusned_size == 0)
		return 0;

	LoopBuff_Write_Item(g_UART1_Tx_Buff,data);
	UART1_EnableTxInt(1);
	return 1;
}
uint8_t UART1_Read(uint8_t data[], uint8_t data_len)
{
	uint8_t len = 0;
	uint16_t i;
	uint16_t unusned_size = LoopBuff_Item_CanRead(g_UART1_Rx_Buff);
	if(unusned_size == 0)
	{
		len = 0;
	}
	else
	{
		for(i = 0; i < data_len; i++)
		{
			if(UART1_ReadByte(&data[i]) == 0)
			{
				break;
			}
		}
		len = i;
	}
	return len;
}

uint16_t UART1_Write(uint8_t data[], uint8_t data_len)
{
	uint16_t i;
	uint16_t unusned_size = LoopBuff_Item_CanWrite(g_UART1_Tx_Buff);
	if (unusned_size < data_len)
	{
		return 0;	// tx缓冲区满，不发送
	}

	for(i = 0; i < data_len; i++)
	{
		LoopBuff_Write_Item(g_UART1_Tx_Buff,data[i]);
	}

	if (g_UART1_Inited)
	UART1_EnableTxInt(1);
	
	return data_len;
}

