#ifndef __Mid_Debug_h__
#define __Mid_Debug_h__

#include "types.h"

typedef struct tagNvI2cDebug_t{
	uint32_t i2c_valid;	/* flag */
	uint32_t i2c_device;	/* mlx = 0x2C, dsp = 0x76 */
	uint32_t i2c_wr;		/* r = 0, wr = 1 */
	uint32_t i2c_addr;	/* address, mlx address format is 0x0000, dsp address format is 0x00 */
	uint32_t i2c_len;
	uint32_t i2c_data;
	
} NvI2cDebug_t;







#ifndef NLOG
	#define DBUG_OUT(type,tag, formate, ...)			Mid_Debug_Sprintf(type, tag, formate, ##__VA_ARGS__)
	#define DBUG_OUT_U8_ARRAY(type,tag, arr,arr_len)	Mid_Debug_Sprintf_u8Array(type,tag, arr,arr_len)
	#define DBUG_OUT_U16_ARRAY(type,tag, arr,arr_len)	Mid_Debug_Sprintf_u16Array_HL(type,tag, arr,arr_len)
#else
	#define DBUG_OUT(type,tag, formate, ...)		 	(void*)0
	#define DBUG_OUT_U8_ARRAY(type,tag, arr,arr_len) 	(void*)0
	#define DBUG_OUT_U16_ARRAY(type,tag, arr,arr_len) 	(void*)0
#endif




void mid_debug_init(void);
void mid_debug_deinit(void);
void Mid_Debug_10msTimer(void);
void Mid_Debug_Service(void);

void Mid_Debug_Sprintf(ELogType_t log_type, uint8_t * log_tag, uint8_t *pcString, ...);
uint8_t Mid_Debug_Sprintf_u8Array(ELogType_t log_type, uint8_t * log_tag, uint8_t arr[], uint8_t len);
uint8_t Mid_Debug_Sprintf_u16Array_HL(ELogType_t log_type, uint8_t * log_tag,uint16_t arr[], uint8_t len);
uint8_t Mid_Debug_Sprintf_u16Array_LH(ELogType_t log_type, uint8_t * log_tag,uint16_t arr[], uint8_t len);
void Mid_Debug_ForNvCam(NvI2cDebug_t *i2c_cmd);


// for test
uint32_t mid_debug_printf_string(uint8_t str[]);
uint32_t mid_debug_printf_u32(uint32_t value, uint32_t min_len);


#endif
