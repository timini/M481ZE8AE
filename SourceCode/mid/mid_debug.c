/**********************************************************************************
 *@file    : debug_printf.c 
 *@brief   : 调试信息接口
 *@author  : mcu
 *@version : Ver 1.1
 *@data    : 2018-03-23
 *@note    : Copyright(c) 2018 JJI. All rights reserved.
 ***********************************************************************************/
 
#include "mid_debug.h"
#include "mid_uart1.h"
#include "mid_systimer.h"


#include "string.h"
#include "stdarg.h"

// ------------------------------------------------------------------------------------------
// 目前调试口为 串口1
uint8_t Debug_Uart_ReadByte(uint8_t *data)
{
	return 0;
}

#define Debug_Uart_EnableTxInt		UART1_EnableTxInt
#define Debug_Uart_Init				UART1_Init
#define Debug_Uart_DeInit			UART1_DeInit
//#define Debug_Uart_ReadByte		UART1_ReadByte
#define Debug_Uart_WriteByte		UART1_WriteByte
#define Debug_Uart_Write			UART1_Write


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// mid层

void mid_debug_init(void)
{
	Debug_Uart_Init(115200);
}


void mid_debug_deinit(void)
{
	Debug_Uart_DeInit();
}
uint32_t mid_debug_printf_string(uint8_t str[])
{
	uint8_t len = 0;
	
	for(len = 0; len < 255; len++)
	{
		if (str[len] == '\0') break;
	}
	return Debug_Uart_Write(str, len);
}

uint32_t mid_debug_printf_u32(uint32_t value, uint32_t min_len)
{
	uint8_t buff[10] = {'0','0','0','0','0','0','0','0','0','0'};
	uint8_t index = 0;
	uint8_t str_len = 0;

	for(uint8_t i = sizeof(buff); i > 0; i--)
	{
		index = i - 1;
		buff[index] = '0' + (value % 10);
		str_len++;
		
		value /= 10;
		if (value == 0)
			break;
	}
	
	if ((min_len <= sizeof(buff)) && (str_len < min_len))
	{
		str_len = min_len;
		index = sizeof(buff) - str_len;
	}
		
	return Debug_Uart_Write(&buff[index], str_len);
}

// 向串口输出log前缀
void mid_debug_log_header(ELogType_t type, uint8_t * tag)
{
	// 1. 输出log_tick
	mid_systime_t time;
	mid_systimer_get_systime(&time);
	Debug_Uart_WriteByte('[');
	mid_debug_printf_u32(time.hour,2);
	Debug_Uart_WriteByte(':');
	mid_debug_printf_u32(time.min,2);
	Debug_Uart_WriteByte(':');
	mid_debug_printf_u32(time.sec,2);
	Debug_Uart_WriteByte('.');
	mid_debug_printf_u32(time.msec,3);
	Debug_Uart_WriteByte(']');


	// 2. 输出 log_type
	mid_debug_printf_string("[");
	if (type & ELOG_Assert)			mid_debug_printf_string(Str_ELOG_Assert);
	else if (type & ELOG_Error)		mid_debug_printf_string(Str_ELOG_Error);
	else if (type & ELOG_Warning)	mid_debug_printf_string(Str_ELOG_Warning);
	else if (type & ELOG_Trace)		mid_debug_printf_string(str_ELOG_Trace);
	else if (type & ELOG_Info)		mid_debug_printf_string(Str_ELOG_Info);
	else if (type & ELOG_Debug)		mid_debug_printf_string(Str_ELOG_Debug);
	else if (type & ELOG_Test)		mid_debug_printf_string(Str_ELOG_Test);
	else 							mid_debug_printf_string(Str_ELOG_Unknown);
	mid_debug_printf_string("]");

	
	// 3.输出log_tag
	mid_debug_printf_string("[");
	mid_debug_printf_string(tag);
	mid_debug_printf_string("]");
}

void Mid_Debug_Sprintf(ELogType_t log_type, uint8_t * log_tag, uint8_t *pcString, ...)
	{
		const uint8_t * g_pcHex = "0123456789abcdef";
		uint32_t ulIdx, ulValue, ulPos, ulCount, ulBase, ulNeg;
		uint8_t *pcStr, pcBuf[16], cFill;

		//return ;
	
		// 输出 log前缀
		mid_debug_log_header(log_type,log_tag);
	
		// 输出 log内容
		va_list vaArgP;
		va_start(vaArgP, pcString);
		while (*pcString)
		{
			//
			// Find the first non-% character, or the end of the string.
			//
			for (ulIdx = 0; (pcString[ulIdx] != '%') && (pcString[ulIdx] != '\0');
				 ulIdx++)
			{
			}
			
			//
			// Write this portion of the string.
			//
			Debug_Uart_Write(pcString,ulIdx); //MidUART_Send2Buf(MID_UART_PRINTF, pcString, ulIdx);
			
			//
			// Skip the portion of the string that was written.
			//
			pcString += ulIdx;
			
			//
			// See if the next character is a %.
			//
			if (*pcString == '%')
			{
				//
				// Skip the %.
				//
				pcString++;
				
				//
				// Set the digit count to zero, and the fill character to space
				// (i.e. to the defaults).
				//
				ulCount = 0;
				cFill = ' ';
				
				//
				// It may be necessary to get back here to process more characters.
				// Goto's aren't pretty, but effective.  I feel extremely dirty for
				// using not one but two of the beasts.
				//
	again:
	
				//
				// Determine how to handle the next character.
				//
				switch (*pcString++)
				{
					//
					// Handle the digit characters.
					//
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					{
						//
						// If this is a zero, and it is the first digit, then the
						// fill character is a zero instead of a space.
						//
						if ((pcString[-1] == '0') && (ulCount == 0))
						{
							cFill = '0';
						}
						
						//
						// Update the digit count.
						//
						ulCount *= 10;
						ulCount += pcString[-1] - '0';
						
						//
						// Get the next character.
						//
						goto again;
					}
					
					//
					// Handle the %c command.
					//
					case 'c':
					{
						//
						// Get the value from the varargs.
						//
						ulValue = va_arg(vaArgP, unsigned long);
						
						//
						// Print out the character.
						//
						Debug_Uart_Write((uint8_t *)&ulValue, 1); //MidUART_Send2Buf(MID_UART_PRINTF, (int8_t *)&ulValue, 1);
						
						//
						// This command has been handled.
						//
						break;
					}
					
					//
					// Handle the %d command.
					//
					case 'd':
					{
						//
						// Get the value from the varargs.
						//
						ulValue = va_arg(vaArgP, unsigned long);
						
						//
						// Reset the buffer position.
						//
						ulPos = 0;
						
						//
						// If the value is negative, make it positive and indicate
						// that a minus sign is needed.
						//
						if ((long)ulValue < 0)
						{
							//
							// Make the value positive.
							//
							ulValue = -(long)ulValue;
							
							//
							// Indicate that the value is negative.
							//
							ulNeg = 1;
						}
						else
						{
							//
							// Indicate that the value is positive so that a minus
							// sign isn't inserted.
							//
							ulNeg = 0;
						}
						
						//
						// Set the base to 10.
						//
						ulBase = 10;
						
						//
						// Convert the value to ASCII.
						//
						goto convert;
					}
					
					//
					// Handle the %s command.
					//
					case 's':
					{
						//
						// Get the string pointer from the varargs.
						//
						pcStr = va_arg(vaArgP, uint8_t *);
						
						//
						// Determine the length of the string.
						//
						for (ulIdx = 0; pcStr[ulIdx] != '\0'; ulIdx++)
						{
						}
						
						//
						// Write the string.
						//
						Debug_Uart_Write(pcStr, ulIdx);//MidUART_Send2Buf(MID_UART_PRINTF, pcStr, ulIdx);
						
						//
						// Write any required padding spaces
						//
						if (ulCount > ulIdx)
						{
							ulCount -= ulIdx;
							while (ulCount--)
							{
								Debug_Uart_Write(" ", 1); //MidUART_Send2Buf(MID_UART_PRINTF, " ", 1);
							}
						}
						//
						// This command has been handled.
						//
						break;
					}
					
					//
					// Handle the %u command.
					//
					case 'u':
					{
						//
						// Get the value from the varargs.
						//
						ulValue = va_arg(vaArgP, unsigned long);
						
						//
						// Reset the buffer position.
						//
						ulPos = 0;
						
						//
						// Set the base to 10.
						//
						ulBase = 10;
						
						//
						// Indicate that the value is positive so that a minus sign
						// isn't inserted.
						//
						ulNeg = 0;
						
						//
						// Convert the value to ASCII.
						//
						goto convert;
					}
					
					//
					// Handle the %x and %X commands.  Note that they are treated
					// identically; i.e. %X will use lower case letters for a-f
					// instead of the upper case letters is should use.  We also
					// alias %p to %x.
					//
					case 'x':
					case 'X':
					case 'p':
					{
						//
						// Get the value from the varargs.
						//
						ulValue = va_arg(vaArgP, unsigned long);
						
						//
						// Reset the buffer position.
						//
						ulPos = 0;
						
						//
						// Set the base to 16.
						//
						ulBase = 16;
						
						//
						// Indicate that the value is positive so that a minus sign
						// isn't inserted.
						//
						ulNeg = 0;
						
						//
						// Determine the number of digits in the string version of
						// the value.
						//
	convert:
						for (ulIdx = 1;
							 (((ulIdx * ulBase) <= ulValue) &&
							  (((ulIdx * ulBase) / ulBase) == ulIdx));
							 ulIdx *= ulBase, ulCount--)
						{
						}
						
						//
						// If the value is negative, reduce the count of padding
						// characters needed.
						//
						if (ulNeg)
						{
							ulCount--;
						}
						
						//
						// If the value is negative and the value is padded with
						// zeros, then place the minus sign before the padding.
						//
						if (ulNeg && (cFill == '0'))
						{
							//
							// Place the minus sign in the output buffer.
							//
							pcBuf[ulPos++] = '-';
							
							//
							// The minus sign has been placed, so turn off the
							// negative flag.
							//
							ulNeg = 0;
						}
						
						//
						// Provide additional padding at the beginning of the
						// string conversion if needed.
						//
						if ((ulCount > 1) && (ulCount < 16))
						{
							for (ulCount--; ulCount; ulCount--)
							{
								pcBuf[ulPos++] = cFill;
							}
						}
						
						//
						// If the value is negative, then place the minus sign
						// before the number.
						//
						if (ulNeg)
						{
							//
							// Place the minus sign in the output buffer.
							//
							pcBuf[ulPos++] = '-';
						}
						
						//
						// Convert the value into a string.
						//
						for (; ulIdx; ulIdx /= ulBase)
						{
							pcBuf[ulPos++] = g_pcHex[(ulValue / ulIdx) % ulBase];
						}
						
						//
						// Write the string.
						//
						Debug_Uart_Write(pcBuf, ulPos);//MidUART_Send2Buf(MID_UART_PRINTF, pcBuf, ulPos);
						
						//
						// This command has been handled.
						//
						break;
					}
					
					//
					// Handle the %% command.
					//
					case '%':
					{
						//
						// Simply write a single %.
						//
						Debug_Uart_Write(pcString - 1, 1); //MidUART_Send2Buf(MID_UART_PRINTF, pcString - 1, 1);
						
						//
						// This command has been handled.
						//
						break;
					}
					
					//
					// Handle all other commands.
					//
					default:
					{
						//
						// Indicate an error.
						//
						Debug_Uart_Write("ERROR", 5);//MidUART_Send2Buf(MID_UART_PRINTF, "ERROR", 5);
						
						//
						// This command has been handled.
						//
						break;
					}
				}
			}
		}

		
		mid_debug_printf_string("\r\n");
		Debug_Uart_EnableTxInt(1);
		
		//
		// End the varargs processing.
		//
		va_end(vaArgP); 
	}


const uint8_t hex_table[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
uint8_t Mid_Debug_Sprintf_u8Array(ELogType_t log_type, uint8_t * log_tag, uint8_t arr[], uint8_t len)
{
	
	uint8_t data_h,data_l;
	uint16_t i;

	// 输出 log前缀
	mid_debug_log_header(log_type, log_tag);

	// 输出 log内容
	for(i = 0; i < len; i++)
	{
		data_h = (uint8_t)((arr[i] >> 4) & 0x0F);
		data_l = (uint8_t)((arr[i] >> 0) & 0x0F);
		Debug_Uart_WriteByte(hex_table[data_h]);
		Debug_Uart_WriteByte(hex_table[data_l]);
		Debug_Uart_WriteByte(' ');
	}
	Debug_Uart_WriteByte('\r');
	Debug_Uart_WriteByte('\n');
	Debug_Uart_EnableTxInt(1);
	return i;
}

uint8_t Mid_Debug_Sprintf_u16Array_HL(ELogType_t log_type, uint8_t * log_tag,uint16_t arr[], uint8_t len)
{
	uint8_t data_h,data_l;
	uint16_t i;

	// 输出 log前缀
	mid_debug_log_header(log_type, log_tag);

	// 输出 log内容
	for(i = 0; i < len; i++)
	{
		data_h = (uint8_t)((arr[i] >> 12) & 0x0F);
		data_l = (uint8_t)((arr[i] >> 8) & 0x0F);
		Debug_Uart_WriteByte(hex_table[data_h]);
		Debug_Uart_WriteByte(hex_table[data_l]);

		data_h = (uint8_t)((arr[i] >> 4) & 0x0F);
		data_l = (uint8_t)((arr[i] >> 0) & 0x0F);
		Debug_Uart_WriteByte(hex_table[data_h]);
		Debug_Uart_WriteByte(hex_table[data_l]);
		Debug_Uart_WriteByte(' ');
	}
	Debug_Uart_EnableTxInt(1);
	return i;
}

uint8_t Mid_Debug_Sprintf_u16Array_LH(ELogType_t log_type, uint8_t * log_tag,uint16_t arr[], uint8_t len)
{
	uint8_t data_h,data_l;
	uint16_t i;

	// 输出 log前缀
	mid_debug_log_header(log_type, log_tag);

	// 输出 log内容
	for(i = 0; i < len; i++)
	{
		data_h = (uint8_t)((arr[i] >> 4) & 0x0F);
		data_l = (uint8_t)((arr[i] >> 0) & 0x0F);
		Debug_Uart_WriteByte(hex_table[data_h]);
		Debug_Uart_WriteByte(hex_table[data_l]);

		data_h = (uint8_t)((arr[i] >> 12) & 0x0F);
		data_l = (uint8_t)((arr[i] >> 8) & 0x0F);
		Debug_Uart_WriteByte(hex_table[data_h]);
		Debug_Uart_WriteByte(hex_table[data_l]);
		
		Debug_Uart_WriteByte(' ');
	}
	Debug_Uart_EnableTxInt(1);
	return i;
}

// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// str_lib

void string_to_lower(uint8_t str[100])
{
	uint8_t i;
	for(i = 0; i < 100; i++)
	{
		if (str[i] == '\0')
			break;
		
		if ((str[i] >= 'A') && (str[i] <= 'Z'))
		{
			str[i] = 'a' + (str[i] - 'A');
		}
	}
}

uint8_t string_startwith(uint8_t str_src[100], uint8_t str_start[])
{
	uint8_t len;
	uint8_t flag = 0;

	for(len = 0; len < 100; len++)
	{
		if (str_src[len] == '\0')
		{
			if(str_start[len] == '\0')
				flag = 1;
			break;
		}
		
		if (str_start[len] == '\0')
		{
			flag = 1;
			break;
		}
		
		if (str_src[len] != str_start[len])
			break;
	}
	return flag;
}



uint8_t string_to_u32(uint8_t str[], uint32_t * data)
{
	uint8_t i;
	uint32_t data32 = 0;
	uint8_t error = 1;

	for(i = 0; i < 8; i++)
	{
		if ((str[i] >= '0') && (str[i] <= '9'))
		{
			data32 <<= 4;
			data32 |= 0x00 + str[i] - '0';
		}
		else if ((str[i] >= 'a') && (str[i] <= 'f'))
		{
			data32 <<= 4;
			data32 |= 0x0a + str[i] - 'a';
		}
		else
		{
			break;
		}
	}
	if (i == 0)
		return error;
	
	*data = data32;
	error = 0;
	return error;
}

uint8_t string_get_param(uint8_t str[100], uint8_t * param_start, uint8_t * param_len)
{
	uint8_t i,j,start = 0,len = 0;
	uint8_t error = 1;

	// 确定参数起始
	for(i = 0; i < 100; i++)
	{
		if (str[i] == ' ' || str[i] == '\t')
			continue;

		start = i;
		break;
	}
	if (i == 100) 
		return error;

	// 确定参数长度
	for(j = i; j < 100; j++)
	{
		if (str[j] == ' ' || str[j] == '\t' || str[j] == '\0')
			break;
		
		len++;
	}
	if (i == 100 || len == 0) 
		return error;

	*param_start = start;
	*param_len = len;
	error = 0;
	return error;	
}


