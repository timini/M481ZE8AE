
#include "mid_spi0.h"
#include "M480.h"
#include "types.h"
#include "gpio.h"

LoopBuf_u16_1024_Define g_SPI0_Tx_Buff;
LoopBuf_u16_1024_Define g_SPI0_Rx_Buff;



void SPI0_Init(void)
{
	/* Unlock protected registers */
    SYS_UnlockReg();
    
	/* Select PCLK as the clock source of SPI0 and SPI1 */
    //CLK_SetModuleClock(SPI0_MODULE, CLK_CLKSEL2_SPI0SEL_PCLK1, MODULE_NoMsk);

    /* Enable SPI0 peripheral clock */
    CLK_EnableModuleClock(SPI0_MODULE);

    /* Enable SPI0 clock pin (PB14) schmitt trigger */ // 施米特触发器???
    PB->SMTEN |= GPIO_SMTEN_SMTEN14_Msk;

    /* Enable SPI0 I/O high slew rate */
    GPIO_SetSlewCtl(PB, 0xF, GPIO_SLEWCTL_HIGH);

    /* Setup SPI0 multi-function pins */
    SYS->GPB_MFPH |= SYS_GPB_MFPH_PB12MFP_SPI0_MOSI | SYS_GPB_MFPH_PB13MFP_SPI0_MISO | SYS_GPB_MFPH_PB14MFP_SPI0_CLK | SYS_GPB_MFPH_PB15MFP_SPI0_SS;

	// Pin Config	//TODO:是否需要配置io口方向
    GPIO_SetMode(PB, BIT12, GPIO_MODE_OUTPUT);//PB.2 UART1_RXD I MFP6 UART1 data receiver input pin.
    GPIO_SetMode(PB, BIT13, GPIO_MODE_INPUT);//PB.3 UART1_TXD O MFP6 UART1 data transmitter output pin.
    GPIO_SetMode(PB, BIT14, GPIO_MODE_OUTPUT);//PB.2 UART1_RXD I MFP6 UART1 data receiver input pin.
    GPIO_SetMode(PB, BIT15, GPIO_MODE_OUTPUT);//PB.3 UART1_TXD O MFP6 UART1 data transmitter output pin.


    /*---------------------------------------------------------------------------------------------------------*/
    /* Init SPI                                                                                                */
    /*---------------------------------------------------------------------------------------------------------*/
    /* Configure as a master, clock idle low, 16-bit transaction, drive output on falling clock edge and latch input on rising edge. */
    /* Set IP clock divider. SPI clock rate = 2MHz */
    SPI_Open(SPI0, SPI_MASTER, SPI_MODE_0, 16, 2000000);

    /* Enable the automatic hardware slave select function. Select the SS pin and configure as low-active. */
    SPI_EnableAutoSS(SPI0, SPI_SS, SPI_SS_ACTIVE_LOW);



	/* Set TX FIFO threshold, enable TX FIFO threshold interrupt and RX FIFO time-out interrupt */
    SPI_SetFIFO(SPI0, 2, 2);
    SPI_EnableInt(SPI0, SPI_FIFO_TXTH_INT_MASK | SPI_FIFO_RXTO_INT_MASK);
    NVIC_EnableIRQ(SPI0_IRQn);

    SYS_LockReg();
}


void SPI0_DeInit(void)
{
	/* Disable TX FIFO threshold interrupt and RX FIFO time-out interrupt */
    SPI_DisableInt(SPI0, SPI_FIFO_TXTH_INT_MASK | SPI_FIFOCTL_RXTOIEN_Msk);
    NVIC_DisableIRQ(SPI0_IRQn);

    /* Reset SPI0 */
    SPI_Close(SPI0);

    //TODO: IO口去初始化
    
}

void SPI0_EnableTxIRQ(uint32_t en)
{
	if (en) SPI_EnableInt(SPI0, SPI_FIFO_TXTH_INT_MASK);
	else    SPI_DisableInt(SPI0, SPI_FIFO_TXTH_INT_MASK);
}

void SPI0_IRQHandler(void)
{
    /* Check RX EMPTY flag */
    while(SPI_GET_RX_FIFO_EMPTY_FLAG(SPI0) == 0)
    {
        /* Read RX FIFO */
        uint16_t rx_data = (uint16_t)SPI_READ_RX(SPI0);
        if (LoopBuff_Is_Full(g_SPI0_Rx_Buff))
        {
        	//TODO:这里记录一下溢出错误
	    	continue;
	    }

	    LoopBuff_Write_Item(g_SPI0_Rx_Buff, rx_data);
    }
    
    /* Check TX FULL flag and TX data count */
    while(SPI_GET_TX_FIFO_FULL_FLAG(SPI0) == 0)
    {
    	if (LoopBuff_Is_Empty(g_SPI0_Tx_Buff))
    	{
			SPI_DisableInt(SPI0, SPI_FIFO_TXTH_INT_MASK); /* Disable TX FIFO threshold interrupt */
    		break;
    	}
    	
        /* Write to TX FIFO */
        uint16_t tx_data = LoopBuff_Read_Item(g_SPI0_Tx_Buff);
        SPI_WRITE_TX(SPI0, tx_data);
    }

    /* Check the RX FIFO time-out interrupt flag */
    if(SPI_GetIntFlag(SPI0, SPI_FIFO_RXTO_INT_MASK)) //????? TODO:查一下这里
    {
        /* If RX FIFO is not empty, read RX FIFO. */
        while((SPI0->STATUS & SPI_STATUS_RXEMPTY_Msk) == 0)
        {
        	/* Read RX FIFO */
	        uint16_t rx_data = (uint16_t)SPI_READ_RX(SPI0);
	        if (LoopBuff_Is_Full(g_SPI0_Rx_Buff))
	        {
	        	//TODO:这里记录一下溢出错误
		    	continue;
		    }

		    LoopBuff_Write_Item(g_SPI0_Rx_Buff, rx_data);
		}
    }
}


uint32_t SPI0_WriteItem(uint16_t data)
{
	uint32_t error = 0;
	
	if (LoopBuff_Is_Full(g_SPI0_Tx_Buff))
		return 1;
		
	LoopBuff_Write_Item(g_SPI0_Tx_Buff, data);

	SPI0_EnableTxIRQ(1);
	return error;
}

void SPI0_Test(void)
{
	SPI0_WriteItem(0x2703);
}











