#ifndef __mid_uart1_h__
#define __mid_uart1_h__

#include "types.h"

void UART1_EnableTxInt(uint8_t enable);
void UART1_Init(uint32_t baud);
void UART1_DeInit(void);
uint8_t UART1_ReadByte(uint8_t * data);
uint8_t UART1_WriteByte(uint8_t data);
uint8_t UART1_Read(uint8_t data[], uint8_t data_len);
uint16_t UART1_Write(uint8_t data[], uint8_t data_len);

void UART1_Test(void);

#endif



