#ifndef __mid_terminal_h__
#define __mid_terminal_h__

#include <stdint.h>

void mid_terminal_init(uint8_t iic_index);
void mid_terminal_deinit(void);
void mid_terminal_service(void);


// for cmd line
void tm_parse_command(uint8_t in_data);


#endif

