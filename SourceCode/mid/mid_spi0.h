#ifndef __mid_spi0_h__
#define __mid_spi0_h__

#include "stdint.h"


void SPI0_Init(void);
void SPI0_DeInit(void);
void SPI0_Test(void);



#endif

