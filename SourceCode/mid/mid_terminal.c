 /**********************************************************************************
 *@file    : mid_terminal.c 
 *@brief   : 命令行接口，主要实现了TW终端下位机
 
 *@author  : data        version     author      note
 *           2017-05-16  Ver1.0      Liucheng    first version
 *           2019-02-12  Ver1.1      LiuCheng    支持 1~4字节长的寄存器地址及寄存器值
 
 *@note    : Copyright(c) 2017 PROTRULY. All rights reserved.
 ***********************************************************************************/
 
 #include "mid_systimer.h"
// ------------------------------------------------------------------------------------------
// 连接至TW8836的iic
//#include "mid_iic1.h"

#define TM_IIC_Init(...)			0//mid_iic1_init
#define TM_IIC_DeInit(...)		    0//mid_iic1_deinit
#define TM_IIC_WriteReg(...)		0//mid_iic1_write_reg_xx
#define TM_IIC_ReadReg(...)		    0//mid_iic1_read_reg_xx

// ------------------------------------------------------------------------------------------
// 目前调试口为 串口1

#include "mid_uart1.h"

#define TM_Uart_Init            UART1_Init
#define TM_Uart_DeInit	        UART1_DeInit
#define TM_Uart_ReadByte        UART1_ReadByte
#define TM_Uart_WriteByte       UART1_WriteByte
#define TM_Uart_Write           UART1_Write


// ------------------------------------------------------------------------------------------
// printf类 函数

uint32_t tw_printf_string(uint8_t str[])
{
	uint8_t len = 0;
	
	for(len = 0; len < 255; len++)
	{
		if (str[len] == '\0') break;
	}
	return TM_Uart_Write(str, len);
}

uint32_t tw_printf_u8dec(uint8_t value)
{
	uint8_t out[4];
	uint8_t index = 0;

	uint8_t value_bai = (uint8_t)((value / 100) %10);
	uint8_t value_shi = (uint8_t)((value / 10) % 10);
	uint8_t value_ge   = (uint8_t)((value / 1) % 10);
	if (value_bai > 0) out[index++] = '0' + value_bai;
	if (value_shi > 0) out[index++] = '0' + value_shi;
	out[index++] = '0' + value_ge;

	return TM_Uart_Write(out, index);
}

uint32_t tw_printf_u8hex(uint8_t value)
{
	uint8_t out[3];
	uint8_t index = 0;
	
	uint8_t value_h = (uint8_t)(value >> 4);
	uint8_t value_l = (uint8_t)(value & 0x0F);
	out[index++] = (value_h < 0x0A) ? ('0' + value_h) : ('A' + value_h - 0x0A);
	out[index++] = (value_l < 0x0A) ? ('0' + value_l) : ('A' + value_l - 0x0A);

	return TM_Uart_Write(out,index);
}

uint32_t tw_printf_u8hex_short(uint8_t value)
{
	uint8_t out[3];
	uint8_t index = 0;
	
	uint8_t value_h = (uint8_t)(value >> 4);
	uint8_t value_l = (uint8_t)(value & 0x0F);
	if (value_h) out[index++] = (value_h < 0x0A) ? ('0' + value_h) : ('A' + value_h - 0x0A);
	out[index++] = (value_l < 0x0A) ? ('0' + value_l) : ('A' + value_l - 0x0A);

	return TM_Uart_Write(out,index);
}


void tw_printf_u16hex(uint16_t value16)
{
	tw_printf_u8hex((uint8_t)(value16 >> 8));
	tw_printf_u8hex((uint8_t)(value16 >> 0));
}

void tw_printf_u24hex(uint32_t value32)
{
	tw_printf_u8hex((uint8_t)(value32 >> 16));
	tw_printf_u8hex((uint8_t)(value32 >>  8));
	tw_printf_u8hex((uint8_t)(value32 >>  0));
}


void tw_printf_u32hex(uint32_t value32)
{
	tw_printf_u8hex((uint8_t)(value32 >> 24));
	tw_printf_u8hex((uint8_t)(value32 >> 16));
	tw_printf_u8hex((uint8_t)(value32 >>  8));
	tw_printf_u8hex((uint8_t)(value32 >>  0));
}

void tw_printf_u32hex_short(uint32_t value32)
{
	uint8_t flag_start = 0;
	for(uint8_t i = 0; i < 4; i++)
	{
		uint8_t value = (uint8_t)(value32 >> ((3 - i) * 8));
		if (value == 0 && flag_start == 0)
			continue;

		if (flag_start == 0)
			tw_printf_u8hex_short(value);	
		else
			tw_printf_u8hex(value);

		flag_start = 1;
	}
}

void tw_printf_u32bin(uint32_t value32, uint8_t nbits )
{
	uint8_t out[33];
	uint8_t index = 0;
	
	for(uint8_t i = 1; i <= nbits ; i++)
	{
		out[index++] = '0' + ((value32 >> (nbits -i)) & 0x01);
	}

	out[index++] = '\0';
	tw_printf_string(out);
}


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// string类 函数

// 将字符串中所有字母变为小写
static void string_to_lower(uint8_t str[])
{
	uint8_t i;
	for(i = 0; i < 255; i++)
	{
		if (str[i] == '\0')
			break;
		
		if ((str[i] >= 'A') && (str[i] <= 'Z'))
		{
			str[i] = 'a' + (str[i] - 'A');
		}
	}
}


// 移除字符串中的前导空格
static void string_remove_start_space(uint8_t str[])
{
	uint8_t * result = str;
	uint8_t index = 0;

	for(uint8_t i = 0; i < 255; i++)
	{
		if (index == 0 && str[i] == ' ') //跳过前导空格
			continue;

		result[index++] = str[i];

		if (str[i] == '\0')
			break;
	}
	return;
}

// 移除字符串中的双空格
static void string_remove_double_space(uint8_t str[])
{
	uint8_t * result = str;
	uint8_t index = 0;
	
	for(uint8_t i = 0; i < 255; i++)
	{
		if (str[i] != ' ')
		{
			result[index++] = str[i];
		}
		else
		{
			if (result[index - 1] != ' ')
				result[index++] = ' ';
		}
		if (str[i] == '\0')
			break;
	}
	return;
}


static uint8_t string_compare(uint8_t str_src[], uint8_t str_dts[])
{
	uint8_t is_equal = 0;

	for(uint8_t i = 0; i < 255; i++)
	{
		if (str_src[i] == '\0' && str_dts[i] == '\0')
		{
			is_equal = 1;
			break;
		}
		
		if (str_src[i] != str_dts[i])
			break;
	}
	return is_equal;
}


static uint8_t string_to_u8hex(uint8_t str[], uint8_t * data)
{
	uint8_t num;
	uint32_t data32 = 0;
	uint8_t error = 0;

	for(num = 0; num < 2; num++)
	{
		if ((str[num] >= '0') && (str[num] <= '9'))
		{
			data32 <<= 4;
			data32 |= 0x00 + str[num] - '0';
		}
		else if ((str[num] >= 'a') && (str[num] <= 'f'))
		{
			data32 <<= 4;
			data32 |= 0x0a + str[num] - 'a';
		}
		else
			break;
	}
	
	error = (num == 0) ? 1 : 0;
	*data = (uint8_t)data32;
	return error;
}
//comment this function to void warning
//static uint8_t string_to_u16hex(uint8_t str[], uint16_t * data16)
//{
//	uint8_t num;
//	uint32_t data32 = 0;
//	uint8_t error = 0;

//	for(num = 0; num < 4; num++)
//	{
//		if ((str[num] >= '0') && (str[num] <= '9'))
//		{
//			data32 <<= 4;
//			data32 |= 0x00 + str[num] - '0';
//		}
//		else if ((str[num] >= 'a') && (str[num] <= 'f'))
//		{
//			data32 <<= 4;
//			data32 |= 0x0a + str[num] - 'a';
//		}
//		else
//			break;
//	}
//	
//	error = (num == 0) ? 1 : 0;
//	*data16 = (uint16_t)data32;
//	return error;
//}

static uint8_t string_to_u32hex(uint8_t str[], uint32_t * data32)
{
	uint8_t error = 0;
	uint8_t num;
	uint32_t dat32 = 0;

	for(num = 0; num < 8; num++)
	{
		if ((str[num] >= '0') && (str[num] <= '9'))
		{
			dat32 <<= 4;
			dat32 |= 0x00 + str[num] - '0';
		}
		else if ((str[num] >= 'a') && (str[num] <= 'f'))
		{
			dat32 <<= 4;
			dat32 |= 0x0a + str[num] - 'a';
		}
		else
			break;
	}
	
	error = (num == 0) ? 1 : 0;	
	*data32 = dat32;
	return error;
}

static uint8_t string_to_u32(uint8_t str[], uint32_t * data32)
{
	uint8_t error = 0;
	uint8_t num;
	uint32_t dat32 = 0;

	for(num = 0; num < 8; num++)
	{
		if ((str[num] >= '0') && (str[num] <= '9'))
		{
			dat32 *= 10;
			dat32 += 0x00 + str[num] - '0';
		}
		else
			break;
	}
	
	error = (num == 0) ? 1 : 0;	
	*data32 = dat32;
	return error;
}


// 
static uint8_t string_to_args(uint8_t str[], uint8_t * argv[], uint8_t argc_max)
{
	uint32_t tt; string_to_u32("",&tt); //to void keil's warning
	
	uint8_t argc = 0;
	uint8_t start = 0;
	for(uint8_t i = 0; i < 255; i++)
	{
		if (str[i] == ' ' || str[i] == '\0')
		{
			argv[argc++] = &str[start];
			start = i + 1;
			
			if (str[i] == '\0')
				break;
			if (argc >= argc_max)
				break;
			
			str[i] = '\0';
		}
	}
	return argc;	
}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// TW终端命令

uint8_t g_tw_addr = 0x8a;
uint8_t g_reg_len = 1;
uint8_t g_dat_len = 1;


void TW_Printf_Mark(void)
{
	tw_printf_string("I2C[");
	tw_printf_u8hex(g_tw_addr);
	tw_printf_string("]>");
}


void TW_Printf_RegValue(uint8_t addr, uint32_t reg, uint32_t value, uint32_t error)
{
	tw_printf_string("<R>");
	tw_printf_u8hex(addr);
	
	tw_printf_string("[");
	if (g_reg_len == 1) tw_printf_u8hex(reg);
	else if (g_reg_len == 2) tw_printf_u16hex(reg);
	else if (g_reg_len == 4) tw_printf_u32hex(reg);
	tw_printf_string("]=");

	if (g_dat_len == 1) tw_printf_u8hex(value);
	else if (g_dat_len == 2) tw_printf_u16hex(value);
	else if (g_dat_len == 4) tw_printf_u32hex(value);
	
	if (error)
		tw_printf_string("*");
	else
		tw_printf_string("");
	tw_printf_string("\r\n");
}


// 输出位域的值，格式如下: b7..b5 = 02(010)
void TW_Printf_RegBitValue(uint32_t reg_value, uint32_t bit_start, uint32_t bit_end)
{	
	uint32_t mask = 0;
	for(uint8_t i = 0; i < 32; i++)
	{
		if ((bit_start <= i) && (i <= bit_end))
		{
			mask |= (1ul << i);
		}
	}
	
	uint32_t bit_value = (reg_value & mask) >> bit_start;
	tw_printf_string(", b");
	tw_printf_u8dec(bit_end);
	tw_printf_string("..b");
	tw_printf_u8dec(bit_start);
	tw_printf_string("=");
	tw_printf_u32hex_short(bit_value);

	tw_printf_string("(");
	tw_printf_u32bin(bit_value, bit_end - bit_start + 1);
	tw_printf_string("b)");
}

// 修改当前IIC地址，如：c 8a 1 1回车
int TW_ChangeAddress(uint8_t argc, uint8_t* argv[])
{
	uint32_t addr= 0;
	uint32_t reg_len = 1;
	uint32_t data_len = 1;
	uint8_t error = 0;

	if (argc >=2)
	{
		error += string_to_u32hex(argv[1], &addr);
	}
	if (argc >= 4)
	{
		error += string_to_u32hex(argv[2], &reg_len);
		error += string_to_u32hex(argv[3], &data_len);
	}
	if (error)
	{
		tw_printf_string("Change I2C Address Failed: Can not parse the args!\r\n");
		return 1;
	}
	if ((addr > 0xFF) || (reg_len > 4) || (g_dat_len > 4))
	{
		tw_printf_string("Change I2C Address Failed: the args is invalid!\r\n");
		return 1;
	}
	
	g_tw_addr = (uint8_t)addr;
	g_reg_len = (uint8_t)reg_len;
	g_dat_len = (uint8_t)data_len;
	tw_printf_string("Change iic_addr to 0x");
	tw_printf_u8hex(g_tw_addr);
	tw_printf_string(", ");
	tw_printf_u8dec(g_reg_len);
	tw_printf_string(" byte-wide reg_addr, and ");
	tw_printf_u8dec(g_dat_len);
	tw_printf_string(" byte-wide reg_data.\r\n");
	
	return 0;
}

// 读寄存器, 如: ( 8a ff回车
int TW_ReadReg(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint32_t addr;
	uint32_t reg;

	if (argc >= 3)
	{
		error += string_to_u32hex(argv[1], &addr);
		error += string_to_u32hex(argv[2], &reg);
		if (error == 0)
		{
			uint32_t value = 0xFFFFFFFFUL; // 读取寄存器值
			error = TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
	}
	
	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 简化版读寄存器，不用指定芯片地址，如: r ff回车
int TW_ReadReg_Lite(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr = g_tw_addr;
	uint32_t reg;

	if (argc >= 2)
	{
		error += string_to_u32hex(argv[1], &reg);
		if (error == 0)
		{
			uint32_t value = 0xFFFFFFFFUL; // 读取寄存器值
			error = TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
	}
	
	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 3. 读寄存器组, 如: & 8a 06 08回车, 
int TW_ReadRegGroup(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr;
	uint32_t reg_start;
	uint32_t reg_end;

	if (argc >= 4)
	{
		error += string_to_u8hex(argv[1], &addr);
		error += string_to_u32hex(argv[2], &reg_start);
		error += string_to_u32hex(argv[3], &reg_end);
		if (error == 0)
		{
			for(uint32_t reg = reg_start; reg <= reg_end; reg++)
			{
				uint32_t value = 0xFFFFFFFF; // 读取寄存器值
				error = TM_IIC_ReadReg(addr, reg, &value);
				TW_Printf_RegValue(addr,reg,value,error);
				
				if (reg == reg_end)
					break;
			}
			return 0;
		}
	}

	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 简化版 读寄存器组，不用指定芯片地址，如: d 06 08回车
int TW_ReadRegGroup_Lite(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr = g_tw_addr;
	uint8_t reg_start;
	uint8_t reg_end;

	if (argc >= 3)
	{
		error += string_to_u8hex(argv[1], &reg_start);
		error += string_to_u8hex(argv[2], &reg_end);
		if (error == 0)
		{
			for(uint32_t reg = reg_start; reg <= reg_end; reg++)
			{
				uint32_t value = 0xFFFFFFFF; // 读取寄存器值
				error = TM_IIC_ReadReg(addr, reg, &value);
				TW_Printf_RegValue(addr,reg,value,error);
				
				if (reg == reg_end)
					break;
			}
			return 0;
		}
	}
	
	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 写寄存器，如: ) 8a ff 00回车
int TW_WriteReg(uint8_t argc, uint8_t* argv[])
{
	uint8_t  error = 0;
	uint8_t  addr;
	uint32_t reg;
	uint32_t value;

	if (argc >= 4)
	{
		error += string_to_u8hex(argv[1], &addr);
		error += string_to_u32hex(argv[2], &reg);
		error += string_to_u32hex(argv[3], &value);
		if (error == 0)
		{
			// 将值写入寄存器, 并再次读出
			error += TM_IIC_WriteReg(addr,reg,value);
			value = 0xFFFFFFFF;
			error += TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
	}
		
	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 简化版 写寄存器，不用指定芯片地址，如: w ff 00回车
int TW_WriteReg_Lite(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr = g_tw_addr;
	uint32_t reg;
	uint32_t value;

	if (argc >= 3)
	{
		error += string_to_u32hex(argv[1], &reg);
		error += string_to_u32hex(argv[2], &value);
		if (error == 0)
		{
			// 将值写入寄存器, 并再次读出
			error += TM_IIC_WriteReg(addr,reg,value);
			value = 0xFFFFFFFF;
			error += TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
	}

	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 写寄存器bit，如: b 8a 40 76 80回车, 用于tw上位机
int TW_WriteBit(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr;
	uint32_t reg;
	uint32_t bit;
	uint32_t value;

	if (argc >= 5)
	{
		error += string_to_u8hex(argv[1], &addr);
		error += string_to_u32hex(argv[2], &reg);
		error += string_to_u32hex(argv[3], &bit);
		error += string_to_u32hex(argv[4], &value);
		if (error == 0)
		{
			uint8_t bit_start = (uint8_t)(bit & 0x0F);
			uint8_t bit_end = (uint8_t)(bit >> 4);

			// 读出寄存器的值，并更新相应位
			uint32_t value_old = 0xFFFFFFFFUL;
			error += TM_IIC_ReadReg(addr, reg, &value_old);
			if (error == 0)
			{
				for(uint8_t i = bit_start; i <= bit_end; i++)
				{
					value_old &= (uint8_t)(~(0x01 << i));
				}
				value = value | value_old;
				
				// 将值写入寄存器, 并再次读出
				error += TM_IIC_WriteReg(addr,reg,value);
			}
			
			value = 0xFFFFFFFF;
			error += TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
		
	}

	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}


// 写寄存器位，如: wb        40 30 02回车,将0x40寄存器的bi5:4设置为2，即bit5=1，bit4=0
int TW_WriteBit_Lite(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr = g_tw_addr;
	uint32_t reg;
	uint32_t mask;
	uint32_t value;

	if (argc >= 4)
	{
		error += string_to_u32hex(argv[1], &reg);
		error += string_to_u32hex(argv[2], &mask);
		error += string_to_u32hex(argv[3], &value);
		error += (mask == 0) ? 1 : 0; //mask用来标记bitfiled的，不可以为0
		if (error == 0)
		{
			uint32_t start = 0;
			for(start = 0; start < 32; start++)
			{
				if (((mask >> start) & 0x01) != 0)
					break;
			}
			value <<= start;
			value &= mask;

			// 读出寄存器的值，并更新相应位
			uint32_t value_old = 0xFFFFFFFFUL;
			error += TM_IIC_ReadReg(addr, reg, &value_old);
			if (error == 0)
			{
				value_old &= ~mask;
				value_old |= value;
				
				// 将值写入寄存器, 并再次读出
				error += TM_IIC_WriteReg(addr,reg,value_old);
			}
			
			value = 0xFFFFFFFF;
			error += TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			return 0;
		}
		
	}

	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}

// 读寄存器位，如：rb    40 7 6回车,将读出0x40寄存器，并计算bi7~bit6的值
int TW_ReadBit_Lite(uint8_t argc, uint8_t* argv[])
{
	uint8_t error = 0;
	uint8_t addr = g_tw_addr;
	uint32_t reg;
	uint32_t start;
	uint32_t end;
	uint32_t value;

	if (argc >= 5)
	{
		error += string_to_u32hex(argv[1], &reg);
		error += string_to_u32hex(argv[2], &start);
		error += string_to_u32hex(argv[3], &end);
		if (error == 0)
		{
			uint32_t bit_start = start < end ? start : end;
			uint32_t bit_end = start < end ? end : start;

			// 读出寄存器的值，并更新相应位
			value = 0xFFFFFFFF;
			error += TM_IIC_ReadReg(addr, reg, &value);
			TW_Printf_RegValue(addr,reg,value,error);
			if (error == 0)
			{
				TW_Printf_RegBitValue(value, bit_start, bit_end);
			}
			return 0;
		}
	}

	tw_printf_string("Error: Can not parse the command!\r\n");
	return 1;
}

// 测试所有iic地址，将有ack的iic地址显示出来。  
int TW_DetectAddress(uint8_t argc, uint8_t* argv[])
{
//	tw_printf_string("     0  2  4  6  8  A  C  E"); // first line
//	
//	for(uint16_t addr = 0; addr < 0xFF; addr+=2)
//	{
//		// line head
//		if ((addr & 0x0F) == 0)
//		{
//			tw_printf_string("\r\n");
//			tw_printf_u8hex((uint8_t)addr);
//			tw_printf_string(":");
//		}

//		// detect result
//		tw_printf_string(" ");
//		if (mid_iic1_detect((uint8_t)addr) == 0)
//		{
//			tw_printf_u8hex((uint8_t)addr);
//		}
//		else
//		{
//			tw_printf_string("--");
//		}
//	}
//	tw_printf_string("\r\n");
	return 1;
}



// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// TW终端命令行接口

typedef int (*TW_CmdHandle_t)(uint8_t argc, uint8_t* argv[]);

typedef struct{
	uint8_t *      func_name;
	TW_CmdHandle_t func_call;
}Debug_CmdLine_Define;

// 当前支持的命令行 列表
Debug_CmdLine_Define g_terminal_commands[] = 
{
	//------------------------------------------------
	// tw官方终端使用的命令
	
	// 1. 修改IIC 地址, 如: c 8A回车
	{"c", TW_ChangeAddress},

	// 2. 读寄存器, 如: ( 8a ff回车
	{"(", TW_ReadReg},

	// 3. 读寄存器组, 如: & 8a 06 08回车
	{"&", TW_ReadRegGroup},

	// 4. 写寄存器，如: ) 8a ff 00回车
	{")", TW_WriteReg},

	// 5. 写寄存器bit，如: b 8a 40 76 80回车
	{"b", TW_WriteBit},

	// 6. 简化版 读寄存器，不用指定芯片地址，如: r ff回车
	{"r", TW_ReadReg_Lite},

	// 7. 简化版 读寄存器组，不用指定芯片地址，如: d 06 08回车
	{"d", TW_ReadRegGroup_Lite},

	// 8. 简化版 写寄存器，不用指定芯片地址，如: w ff 00回车
	{"w", TW_WriteReg_Lite},

	//-----------------------------------------------------
	// 自定义的辅助命令

	{"det", TW_DetectAddress},
	
	// 读写寄存器位，如：wb 40 7 6 2回车
	{"wb", TW_WriteBit_Lite},

	// 读寄存器位，如：rb 40 7 6回车
	{"rb", TW_ReadBit_Lite},

	
};

#define CmdLine_Len_Max		255
uint8_t g_debug_cmdline[CmdLine_Len_Max];
uint8_t g_debug_cmdline_len;

uint8_t  g_argc = 0;
uint8_t* g_argv[20];

uint8_t string_startwith(uint8_t str_src[100], uint8_t str_start[]);

void tm_proc_command(void)
{
	uint8_t i;
	uint8_t cmd_num = sizeof(g_terminal_commands) / sizeof(Debug_CmdLine_Define);

	if (g_debug_cmdline_len == 0)
		return;

	string_to_lower(g_debug_cmdline);
	string_remove_start_space(g_debug_cmdline);
	string_remove_double_space(g_debug_cmdline);
	g_argc = string_to_args(g_debug_cmdline, &g_argv[0], sizeof(g_argv)/sizeof(uint8_t *));
	for(i = 0; i <cmd_num; i++)
	{
		if (string_compare(g_argv[0],g_terminal_commands[i].func_name))
		{
			g_terminal_commands[i].func_call(g_argc, g_argv);
			break;
		}
	}
	if (i == cmd_num)
	{
		tw_printf_string("Error, Can not find the command!\r\n");
	}
}


// 组帧
void tm_parse_command(uint8_t in_data)
{
	switch(in_data)
	{
		case 0x1b:
		case '\r':
		case '\n':
		case '+': //+被视为回车，用于一行输入多条命令
			tw_printf_string("\r\n");
			tm_proc_command();
			
			//tw_printf_string("\r\n");
			TW_Printf_Mark();
			
			g_debug_cmdline_len = 0;
			g_debug_cmdline[g_debug_cmdline_len] = '\0';
		break;
		case 0x08:		// 退格
			if (g_debug_cmdline_len > 0)
			{
				g_debug_cmdline[g_debug_cmdline_len - 1] = '\0';
				g_debug_cmdline_len--;
				
				TM_Uart_WriteByte(0x08);
				TM_Uart_WriteByte(' ');
				TM_Uart_WriteByte(0x08);
			}
		break;
		default:		// 其它字符
			if (g_debug_cmdline_len < CmdLine_Len_Max)
			{
				g_debug_cmdline[g_debug_cmdline_len++] = in_data;
				g_debug_cmdline[g_debug_cmdline_len] = '\0';
				
				TM_Uart_WriteByte(in_data);
			}
		break;
	}
}


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// mid层

void mid_terminal_init(uint8_t iic_index)
{
	//TM_Uart_Init(115200);
	//TM_IIC_Init(iic_index);
}

void mid_terminal_deinit(void)
{
	//TM_Uart_DeInit();
	//TM_IIC_DeInit();
}


void mid_terminal_service(void)
{
	uint8_t in_data;

	// 解析 节点监视消息
	if (TM_Uart_ReadByte(&in_data))
	{
		tm_parse_command(in_data);
	}
}




